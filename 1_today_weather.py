from pygismeteo import Gismeteo
from datetime import date
import locale

locale.setlocale(locale.LC_TIME, 'ru_RU.UTF-8')


def check_weather(city: str) -> str:
    gismeteo = Gismeteo()
    day = date.today().day
    month = date.today().strftime("%B")
    search_results = gismeteo.search.by_query(city)
    city_id = search_results[0].id
    current = gismeteo.current.by_id(city_id)
    temperature = current.temperature.air.c
    if temperature < 0:
        return f'Сегодня {day} {month}. На улице {temperature} градусов.\nХолодно, лучше остаться дома'
    return f'Сегодня {day} {month}. На улице {temperature} градусов.'


if __name__ == '__main__':
    city = input('Введите ваш город: ')
    print(check_weather(city))
