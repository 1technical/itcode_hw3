def fib_number(number: int) -> int:
    if number == 0:
        return 0
    if number == 1:
        return 1
    return fib_number(number - 1) + fib_number(number - 2)


if __name__ == '__main__':
    number = int(input())
    print(fib_number(number))
