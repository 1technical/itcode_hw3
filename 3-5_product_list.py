product_list = ['Картофель',
                'Морковь',
                'Лук',
                'Чеснок',
                'Петрушка',
                'Укроп',
                'Яблоки',
                'Лимон']


def count_products(products: list) -> int:
    count = len(products)
    return count


def show_products(products: list) -> list:
    return [item for item in products]


def even_name_product(products: list) -> list:
    return [name for name in products if len(name) % 2 == 0]


if __name__ == '__main__':
    print(count_products(product_list))
    print(*show_products(product_list), sep=", ")
    print(*even_name_product(product_list), sep=", ")
