def speed_of_light():
    speed_kmh = 1079252848.8
    speed_kms = int(speed_kmh / 3600)
    return f'Скорость света равна {speed_kms} км/с'


if __name__ == '__main__':
    print(speed_of_light())
